# dbwrapper

Database Wrapper storing XML Templates to upload Measurement Data for OT Module Assembly
Collection of useful methods to handle the communication and data upload with the CMS Phase 2 construction DB

Example:
```
from dbwrapper.dbwrapper import DBWrapper

base_url    = "https://cmsomsdet.cern.ch/tracker-resthub/query"
upload_url  = "https://cmsdca.cern.ch/trk_loader/trker/int2r"

theWrapper = DBWrapper( base_url, upload_url )
moduleIvExample["runNumber"] = theWrapper.getRunNumber() #Please see example measurement dictionaries in DBWrapper
theWrapper.generateModuleIvFile(moduleIvExample, "./dbwrapper/temp/module_iv.xml")
theWrapper.uploadFile("./dbwrapper/temp/module_iv.xml")
```