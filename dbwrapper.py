import os, sys, io
from contextlib import redirect_stdout

from .cmsdbldr.src.main.python.cmsdbldr_client import LoaderClient
from .resthub.clients.python.src.main.python.rhapi import CLIClient

from tabulate import tabulate

from matplotlib.patches import Patch
from matplotlib.lines import Line2D

import json
import yaml
import matplotlib as mpl

import matplotlib.pyplot as plt
import numpy as np

from jinja2 import Environment, FileSystemLoader, select_autoescape
env = Environment(
    loader=FileSystemLoader( os.path.dirname(os.path.realpath(__file__)) + "/DatabaseTemplates"),
    autoescape=select_autoescape(['html', 'xml'])
)

CGREEN = '\033[32m'     #Green prints
CRED   = '\033[91m'     #Red prints
CEND   = '\033[0m'      #End colored prints

class DBWrapper:
    def __init__( self, pBaseUrl, pUploadUrl ):
        self.baseUrl = pBaseUrl
        self.uploadUrl = pUploadUrl
        self.rhapiFile = os.getcwd()#              "resthub/clients/python/src/main/python/rhapi.py",
        #print(self.rhapiFile)
        print("New DBWrapper")
        print("\tBase URL: " + self.baseUrl)
        print("\tUpload URL: " + self.uploadUrl)
        print("\tDatabase templates: " + os.path.dirname(os.path.realpath(__file__)) + "/DatabaseTemplates")


    def check_database(self):
        request = "trker_cmsr"
        data = self.getData(request, False)
        if data:
            return len(data) > 0
        else:
            return False


    #------------------------------------------------------------------------
    #------------------------      DOWNLOAD       -----------------------------
    #------------------------------------------------------------------------


    def testUpload(self, doPrint = False):
        testData = None
        with open(os.path.dirname(os.path.realpath(__file__)) + "/DatabaseTemplates/dataExamples.yaml", 'r') as f:
            testData = yaml.safe_load(f)

        print("\nTest sensor IV upload")
        #testData["sensorIvExample"]["runNumber"] = self.getRunNumber()
        if True: #testData["sensorIvExample"]["runNumber"] is not None:
            sensorIvFile = os.path.dirname(os.path.realpath(__file__)) + "/temp/sensor_iv.xml"
            sensorIvFileString = self.generateSensorIvFile(testData["sensorIvExample"], sensorIvFile)

            if doPrint:
                print (sensorIvFileString)
            responseCode = self.uploadFile(sensorIvFile)
            print(self.translateResponseCode(responseCode))
        else:
            print(CRED + "No run number, no upload!" + CEND)
        print("_____________________")

        print("\nTest module IV upload")
        #testData["moduleIvExample"]["runNumber"] = self.getRunNumber()
        if True: #testData["moduleIvExample"]["runNumber"]  is not None:
            moduleIvFile = os.path.dirname(os.path.realpath(__file__)) + "/temp/module_iv.xml"
            moduleIvFileString = self.generateModuleIvFile(testData["moduleIvDemonstator"], moduleIvFile)
            if doPrint:
                print (moduleIvFileString)
            responseCode = self.uploadFile(moduleIvFile)
            print(self.translateResponseCode(responseCode))
        else:
            print(CRED + "No run number, no upload!" + CEND)
        print("_____________________")

        print("\nTest sensor metrology upload")
        #testData["sensorMetrologyExample"]["runNumber"] = self.getRunNumber()
        if True: #testData["sensorMetrologyExample"]["runNumber"] is not None:
            sensorMetrologyFile = os.path.dirname(os.path.realpath(__file__)) + "/temp/sensor_metrology.xml"
            sensorMetrologyFileString = self.generateSensorMetrologyFile(testData["sensorMetrologyExample"], sensorMetrologyFile)
            if doPrint:
                print (sensorMetrologyFileString)
            responseCode = self.uploadFile(sensorMetrologyFile)
            print(self.translateResponseCode(responseCode))
        else:
            print("No upload!")
        print("_____________________")

        print("\nTest module metrology upload")
        #testData["moduleMetrologyExample"]["runNumber"] = self.getRunNumber()
        if True: #testData["moduleMetrologyExample"]["runNumber"] is not None:
            moduleMetrologyFile = os.path.dirname(os.path.realpath(__file__)) + "/temp/module_metrology.xml"
            moduleMetrologyFileString = self.generateModuleMetrologyFile(testData["moduleMetrologyExample"],moduleMetrologyFile)
            if doPrint:
                print (moduleMetrologyFileString)
            responseCode = self.uploadFile(moduleMetrologyFile)
            print(self.translateResponseCode(responseCode))
        else:
            print("No upload!")
        print("_____________________")

        print("\nTest sensor pull test upload")
        #testData["sensorPullTestExample"]["runNumber"] = self.getRunNumber()
        if True: #testData["sensorPullTestExample"]["runNumber"] is not None:
            sensorPullTestFile = os.path.dirname(os.path.realpath(__file__)) + "/temp/sensor_pullTest.xml"
            sensorPullTestFileString = self.generateSensorPullTestFile(testData["sensorPullTestExample"],sensorPullTestFile)
            if doPrint:
                print (sensorPullTestFileString)
            responseCode = self.uploadFile(sensorPullTestFile)
            print(self.translateResponseCode(responseCode))
        else:
            print("No upload!")
        print("_____________________")

        print("\nTest module pull test upload")
        #testData["modulePullTestExample"]["runNumber"] = self.getRunNumber()
        if True: #testData["modulePullTestExample"]["runNumber"] is not None:
            modulePullTestFile = os.path.dirname(os.path.realpath(__file__)) + "/temp/module_pullTest.xml"
            modulePullTestFileString = self.generateModulePullTestFile(testData["modulePullTestExample"],modulePullTestFile)
            if doPrint:
                print (modulePullTestFileString)
            responseCode = self.uploadFile(modulePullTestFile)
            print(self.translateResponseCode(responseCode))
        else:
            print("No upload!")
        print("_____________________")

        print("\nTest missing bond upload")
        #testData["missingBondsExample"]["runNumber"] = self.getRunNumber()
        if True: #testData["missingBondsExample"]["runNumber"] is not None:
            missingBondFile = os.path.dirname(os.path.realpath(__file__)) + "/temp/module_missingBond.xml"
            missingBondFileString = self.generateMissingBondFile(testData["missingBondsExample"], missingBondFile)
            if doPrint:
                print (missingBondFileString)   
            responseCode = self.uploadFile(missingBondFile)
            print(self.translateResponseCode(responseCode))
        else:
            print("No upload!")
        print("_____________________")

    def translateResponseCode( self, pResponseCode):
        translation = str(pResponseCode)
        if pResponseCode == 0:
            translation += ": " + CGREEN + "Upload successfull" + CEND
        elif pResponseCode == 4:
            translation += ": " + CRED +  "Upload failed" +CEND
        else:
            translation += ": Unknown response code"
        return translation

    def getRunNumber( self ):
        query_string = "select r.run_number from trker_cmsr.trk_ot_test_nextrun_v r"
        result = self.getData(query_string)
        return result[0]["runNumber"]

    def generateSensorIvFile( self, pMeasurement, pFile = None):
        template = env.get_template('template_sensor_iv.xml')
        xmlString = template.render ( pMeasurement )
        if pFile is not None:
            with open(pFile, 'w') as file:
                file.write(xmlString)
        return xmlString

    def generateModuleIvFile( self, pMeasurement, pFile = None):
        template = env.get_template('template_module_iv.xml')
        xmlString = template.render ( pMeasurement )
        if pFile is not None:
            with open(pFile, 'w') as file:
                file.write(xmlString)
                print(pFile)
        return xmlString

    def generateSensorMetrologyFile ( self, pMeasurement, pFile = None):
        template = env.get_template('template_sensor_metrology.xml')
        xmlString = template.render ( pMeasurement )
        if pFile is not None:
            with open(pFile, 'w') as file:
                file.write(xmlString)
        return xmlString

    def generateModuleMetrologyFile ( self, pMeasurement, pFile = None):
        template = env.get_template('template_module_metrology.xml')
        xmlString = template.render ( pMeasurement )
        if pFile is not None:
            with open(pFile, 'w') as file:
                file.write(xmlString)
        return xmlString

    def generateSensorPullTestFile ( self, pMeasurement, pFile = None):
        template = env.get_template('template_sensor_bond_pull_test.xml')
        xmlString = template.render ( pMeasurement )
        if pFile is not None:
            with open(pFile, 'w') as file:
                file.write(xmlString)
        return xmlString

    def generateModulePullTestFile ( self, pMeasurement, pFile = None):
        template = env.get_template('template_module_bond_pull_test.xml')
        xmlString = template.render ( pMeasurement )
        if pFile is not None:
            with open(pFile, 'w') as file:
                file.write(xmlString)
        return xmlString     

    def generateMissingBondFile ( self, pMeasurement, pFile = None):
        template = env.get_template('template_missing_bonds.xml')
        xmlString = template.render ( pMeasurement )
        if pFile is not None:
            with open(pFile, 'w') as file:
                file.write(xmlString)
        return xmlString

    def uploadFile( self, pPathToFile ):
        cli = LoaderClient()
        #Prepare command list with upload url, that you need to log in and the file which should be uploaded
        cmd = ['-u', self.uploadUrl, '--login', pPathToFile]
        #Run the upload command
        #print (cmd)
        return cli.run(cmd)

    def demonstrate_module_iv_upload_with_run_number( self, pMeasurement ):
        number = self.getRunNumber()                                        #Get run number from DB and add to your dictionary
        print("Run number:\t" + str ( number ) )
        pMeasurement["runNumber"] = number
        template = env.get_template('template_module_iv_runNumber.xml')     #Get correct template
        xmlString = template.render ( pMeasurement )                        #Render data into xml skeleton
        tempFile = "dbwrapper/temp/tempFileModuleIvUpload.xml" 
        with open(tempFile, 'w') as file:                                   #Write a temporary file on your local disk
            file.write(xmlString)
        cli = LoaderClient()                                                #Generate loader client
        cmd = ['-u', self.uploadUrl, '--login', tempFile]                   #self.uploadUrl = "https://cmsdca.cern.ch/trk_loader/trker/cmsr"
        returnCode = cli.run(cmd) 

    #------------------------------------------------------------------------
    #------------------------      UPLOAD       -----------------------------
    #------------------------------------------------------------------------

    def getData( self, pQuery, pPrintRaw = False):
        #Bypass stdout from print() function in cli.run() to f
        #This is not very stable! If another program prints at the same time in you terminal there might be a clash with the data!!!!
        f = io.StringIO()
        with redirect_stdout(f):
            cli = CLIClient()
            #write system arguments expected by run()
            sys.argv = [ "python3",
                    "-n",
                    "--url=https://cmsdca.cern.ch/trk_rhapi",
                    pQuery,
                    "--login",
                    "-f",
                    "json2",
                    "--all"]
            cli.run()

        result = f.getvalue()
        if result[0:5] == "ERROR":
            return False
        if pPrintRaw:
            print(result)


        #Convert string to python array or dict
        data = json.loads(result.replace("'","\"" ).replace("\r","").replace("\n",""))
        if not isinstance(data, list):
            data = data["data"]
        if len ( data) > 0:
            return data
        else:
            return False 
        

    def getModuleInformation( self, pNameLabel):
        if "2S" in pNameLabel:
            return self.get2SModuleInformation(pNameLabel)
        elif "PS" in pNameLabel:
            return self.getPSModuleInformation(pNameLabel)
        else:
            return self.get2SModuleInformation(pNameLabel)

    def get2SModuleInformation( self, pNameLabel ):
        query = "select pp.NAME_LABEL as MODULE_NAME, pp.A2S_SENSOR_SPACING as SPACING, part.MANUFACTURER, s.A2S_SENSOR_POSN as SENSOR_POSITION, \
                pp.ASTATUS as MODULE_STATUS, pp.DESCRIPTION, r.CHILD_NAME_LABEL AS PART_NAME_LABEL, \
                r.CHILD_SERIAL_NUMBER as PART_SERIAL_NUMBER, feh.AFE_HYBRID_SIDE as FEH_HYBRID_SIDE, feh.ACIC_VERSION as CIC_VERSION, \
                feh.KIND_OF_PART as FEH_KIND_OF_PART, seh.KIND_OF_PART as SEH_KIND_OF_PART, s.KIND_OF_PART as SENSOR_KIND_OF_PART\
                from trker_cmsr.parts part\
                inner join trker_cmsr.p9000 pp on pp.ID = part.ID \
                inner join trker_cmsr.trkr_relationships_v r on r.PARENT_NAME_LABEL=pp.NAME_LABEL\
                left join trker_cmsr.p6640 feh on r.CHILD_SERIAL_NUMBER=feh.SERIAL_NUMBER\
                left join trker_cmsr.p6660 seh on r.CHILD_SERIAL_NUMBER=seh.SERIAL_NUMBER\
                left join trker_cmsr.p1120 s on r.CHILD_NAME_LABEL=s.NAME_LABEL\
                where part.NAME_LABEL='"+pNameLabel+"'"

        data = self.getData(query)
        if data:
            orderedData= self.group2SModuleInformation(data)
            return orderedData[pNameLabel]
        else:
            print("Data not available")
            return False

    def getPSModuleInformation( self, pNameLabel ):
        query = "select pp.NAME_LABEL as MODULE_NAME, pp.APS_SENSOR_SPACING as SPACING, part.MANUFACTURER, pp.ASTATUS as MODULE_STATUS, pp.DESCRIPTION, \
                r.CHILD_NAME_LABEL AS PART_NAME_LABEL, r.CHILD_SERIAL_NUMBER as PART_SERIAL_NUMBER, \
                feh.AFE_HYBRID_SIDE as FEH_HYBRID_SIDE, feh.ACIC_VERSION as CIC_VERSION, feh.KIND_OF_PART as FEH_KIND_OF_PART, \
                roh.KIND_OF_PART as ROH_KIND_OF_PART, poh.KIND_OF_PART as POH_KIND_OF_PART,\
                pss.KIND_OF_PART as PSS_SENSOR_KIND_OF_PART, psp.KIND_OF_PART as PSP_SENSOR_KIND_OF_PART\
                from trker_cmsr.parts part\
                inner join trker_cmsr.p9020 pp on pp.ID = part.ID\
                inner join trker_cmsr.trkr_relationships_v r on r.PARENT_NAME_LABEL=pp.NAME_LABEL\
                left join trker_cmsr.p6740 feh on r.CHILD_SERIAL_NUMBER=feh.SERIAL_NUMBER\
                left join trker_cmsr.p6760 roh on r.CHILD_SERIAL_NUMBER=roh.SERIAL_NUMBER\
                left join trker_cmsr.p10200 poh on r.CHILD_SERIAL_NUMBER=poh.SERIAL_NUMBER\
                left join trker_cmsr.p1160 pss on r.CHILD_NAME_LABEL=pss.NAME_LABEL\
                left join trker_cmsr.p1200 psp on r.CHILD_NAME_LABEL=psp.NAME_LABEL\
                where part.NAME_LABEL='"+pNameLabel+"'" 

        data = self.getData(query)
        print("Data\n")
        print(data)
        if data:
            orderedData= self.groupPSModuleInformation(data)
            return orderedData[pNameLabel]
        else:
            print("Data not available")
            return False

    def print2SModuleTable( self ):
        query = "select pp.NAME_LABEL as MODULE_NAME, pp.A2S_SENSOR_SPACING as SPACING, part.MANUFACTURER, s.A2S_SENSOR_POSN as SENSOR_POSITION, \
                pp.ASTATUS as MODULE_STATUS, pp.DESCRIPTION, r.CHILD_NAME_LABEL AS PART_NAME_LABEL, \
                r.CHILD_SERIAL_NUMBER as PART_SERIAL_NUMBER, feh.AFE_HYBRID_SIDE as FEH_HYBRID_SIDE, feh.ACIC_VERSION as CIC_VERSION, \
                feh.KIND_OF_PART as FEH_KIND_OF_PART, seh.KIND_OF_PART as SEH_KIND_OF_PART, s.KIND_OF_PART as SENSOR_KIND_OF_PART, \
                seh.ALPGBT_VERSION as LPGBT_VERSION \
                from trker_cmsr.parts part\
                inner join trker_cmsr.p9000 pp on pp.ID = part.ID \
                inner join trker_cmsr.trkr_relationships_v r on r.PARENT_NAME_LABEL=pp.NAME_LABEL\
                left join trker_cmsr.p6640 feh on r.CHILD_SERIAL_NUMBER=feh.SERIAL_NUMBER\
                left join trker_cmsr.p6660 seh on r.CHILD_SERIAL_NUMBER=seh.SERIAL_NUMBER\
                left join trker_cmsr.p1120 s on r.CHILD_NAME_LABEL=s.NAME_LABEL\
                order by part.MANUFACTURER, pp.NAME_LABEL"

        data = self.getData(query)

        moduleNames = []
        for entry in data:
            moduleNames.append(entry.get("nameLabel",""))
        moduleNames = list( set (moduleNames ))

        modules = []
        for name in moduleNames:
            modules.append({"Module Name": name})

        request = [ "moduleName","spacing","manufacturer","childNameLabel","sensorKindOfPart","sensorPosition","childSerialNumber","fehHybridSide",
                    "cicVersion","sehKindOfPart","fehKindOfPart","moduleStatus","description"]

        table = [[  entry.get("moduleName",""),
                    entry.get("spacing",""),
                    entry.get("manufacturer",""),
                    entry.get("partNameLabel",""),
                    entry.get("sensorKindOfPart",""),
                    entry.get("sensorPosition",""),
                    entry.get("partSerialNumber",""),
                    entry.get("fehHybridSide",""),
                    entry.get("cicVersion",""),
                    entry.get("lpgbtVersion"),
                    entry.get("sehKindOfPart",""),
                    entry.get("fehKindOfPart",""),
                    entry.get("moduleStatus",""),
                    entry.get("description","")] for entry in data]

        """
        print ( tabulate ( table , headers=["Module Name",
                                            "Spacing",
                                            "Module\nManufacturer",
                                            "Child NameLabel",
                                            "Sensor Type",
                                            "Sensor\nPosition",
                                            "Child SerialNumber",
                                            "Side",
                                            "CIC",
                                            "SEH",
                                            "FEH",
                                            "Module\nStatus",
                                            "Description"],maxcolwidths=[None,None,10,None,None,None,None,None,None,None,10,None,25]))
        """
        groupedData = self.group2SModuleInformation(data)

        header=[    "Module",
                    "Spacing",
                    "Manufacturer",
                    "Top Sensor",
                    "Bottom Sensor",
                    "SEH",
                    "FEH Right",
                    "FEH Left",
                    "CIC",
                    "lpGBT",
                    "Status",
                    "Comment"]

        keys=[  "name",
                "spacing",
                "manufacturer",
                "topSensor",
                "bottomSensor",
                "seh",
                "rightFeh",
                "leftFeh",
                "cicVersion",
                "lpgbtVersion",
                "moduleStatus",
                "description"]

        table = [[groupedData[name].get(key,"") for key in keys] for name in sorted(list(groupedData.keys())) if name[14] =="1" ]
        print("\n")
        print ( tabulate ( table, headers= header, maxcolwidths=[None,None,None,None,None,None,None,None,None,None,29]))

        self.printToCsv("2S_Modules.csv", header, table)


        print ("\nOverall number of 2S Modules in the Database: " + str (len(groupedData.keys()) ) + "\n")

        goodcic1 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC1" and groupedData[name].get("moduleStatus","") == "Good") ]
        badcic1 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC1" and groupedData[name].get("moduleStatus","")  in ["Bad", "Damaged"]) ]
        unknowncic1 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC1" and groupedData[name].get("moduleStatus","") == "") ]
        goodcic2 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2" and groupedData[name].get("moduleStatus","") == "Good") ]
        badcic2 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2" and groupedData[name].get("moduleStatus","")  in ["Bad", "Damaged"]) ]
        unknowncic2 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2" and groupedData[name].get("moduleStatus","") == "") ]
        goodcic21 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2.1" and groupedData[name].get("moduleStatus","") == "Good") ]
        badcic21 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2.1" and groupedData[name].get("moduleStatus","") in ["Bad", "Damaged"]) ]
        unknowncic21 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2.1" and groupedData[name].get("moduleStatus","") == "") ]


        sg1 = ""
        sb1 = ""
        su1 = ""
        sg2 = ""
        sb2 = ""
        su2 = ""
        sg21 = ""
        sb21 = ""
        su21 = ""

        for module in goodcic1:
            sg1 += "\n" + module
        for module in badcic1:
            sb1 += "\n" + module
        for module in unknowncic1:
            su1 += "\n" + module
        for module in goodcic2:
            sg2 += "\n" + module
        for module in badcic2:
            sb2 += "\n" + module
        for module in unknowncic2:
            su2 += "\n" + module
        for module in goodcic21:
            sg21 += "\n" + module
        for module in badcic21:
            sb21 += "\n" + module
        for module in unknowncic21:
            su21 += "\n" + module


        moduleHeader = ["Good CIC1 Modules","Bad CIC1 Modules", "Unknown CIC1 Modules","Good CIC2 Modules","Bad CIC2 Modules", "Unknown CIC2 Modules","Good CIC2.1 Modules","Bad CIC2.1 Modules", "Unknown CIC2.1 Modules"]

        table = []
        table.append([len(goodcic1), len(badcic1), len(unknowncic1),len(goodcic2), len(badcic2), len(unknowncic2),len(goodcic21), len(badcic21), len(unknowncic21)])
        table.append([sg1,sb1,su1,sg2,sb2,su2,sg21,sb21,su21])

        print( tabulate ( table, headers= moduleHeader))

    def printPSModuleTable( self ):

        query = "select pp.NAME_LABEL as MODULE_NAME, pp.APS_SENSOR_SPACING as SPACING, part.MANUFACTURER, pp.ASTATUS as MODULE_STATUS, pp.DESCRIPTION, \
                r.CHILD_NAME_LABEL AS PART_NAME_LABEL, r.CHILD_SERIAL_NUMBER as PART_SERIAL_NUMBER, \
                feh.AFE_HYBRID_SIDE as FEH_HYBRID_SIDE, feh.ACIC_VERSION as CIC_VERSION, feh.KIND_OF_PART as FEH_KIND_OF_PART, \
                roh.KIND_OF_PART as ROH_KIND_OF_PART, roh.ALPGBT_VERSION as LPGBT_VERSION, roh.ALPGBT_BANDWIDTH as LPGBT_BANDWIDTH, poh.KIND_OF_PART as POH_KIND_OF_PART,\
                pss.KIND_OF_PART as PSS_SENSOR_KIND_OF_PART, psp.KIND_OF_PART as PSP_SENSOR_KIND_OF_PART\
                from trker_cmsr.parts part\
                inner join trker_cmsr.p9020 pp on pp.ID = part.ID\
                inner join trker_cmsr.trkr_relationships_v r on r.PARENT_NAME_LABEL=pp.NAME_LABEL\
                left join trker_cmsr.p6740 feh on r.CHILD_SERIAL_NUMBER=feh.SERIAL_NUMBER\
                left join trker_cmsr.p6760 roh on r.CHILD_SERIAL_NUMBER=roh.SERIAL_NUMBER\
                left join trker_cmsr.p10200 poh on r.CHILD_SERIAL_NUMBER=poh.SERIAL_NUMBER\
                left join trker_cmsr.p1160 pss on r.CHILD_NAME_LABEL=pss.NAME_LABEL\
                left join trker_cmsr.p1200 psp on r.CHILD_NAME_LABEL=psp.NAME_LABEL\
                order by part.MANUFACTURER, pp.NAME_LABEL"

        data = self.getData(query)
        print(data)
        moduleNames = []
        for entry in data:
            moduleNames.append(entry.get("nameLabel",""))
        moduleNames = list( set (moduleNames ))

        modules = []
        for name in moduleNames:
            modules.append({"Module Name": name})

        request = [ "moduleName","spacing","manufacturer","childNameLabel","pssSensorKindOfPart","pspSensorKindOfPart","childSerialNumber","fehHybridSide",
                    "cicVersion","rohKindOfPart", "pohKindOfPart","fehKindOfPart","moduleStatus","description"]

        table = [[  entry.get("moduleName",""),
                    entry.get("spacing",""),
                    entry.get("manufacturer",""),
                    entry.get("partNameLabel",""),
                    entry.get("pssSensorKindOfPart",""),
                    entry.get("pspSensorKindOfPart",""),
                    entry.get("partSerialNumber",""),
                    entry.get("fehHybridSide",""),
                    entry.get("cicVersion",""),
                    entry.get("lpgbtVersion",""),
                    entry.get("lpgbtBandwidth",""),
                    entry.get("rohKindOfPart",""),
                    entry.get("pohKindOfPart",""),
                    entry.get("fehKindOfPart",""),
                    entry.get("moduleStatus",""),
                    entry.get("description","")] for entry in data]

        """
        print ( tabulate ( table , headers=["Module Name",
                                            "Spacing",
                                            "Module\nManufacturer",
                                            "Child NameLabel",
                                            "Sensor Type",
                                            "Sensor\nPosition",
                                            "Child SerialNumber",
                                            "Side",
                                            "CIC",
                                            "SEH",
                                            "FEH",
                                            "Module\nStatus",
                                            "Description"],maxcolwidths=[None,None,10,None,None,None,None,None,None,None,10,None,25]))
        """        
        
        groupedData = self.groupPSModuleInformation(data)

        header=[    "Module",
                    "Spacing",
                    "Manufacturer",
                    "PSS Sensor",
                    "PSP Sensor",
                    "ROH",
                    "POH",
                    "FEH Right",
                    "FEH Left",
                    "CIC",
                    "lpGBT",
                    "Bandwidth",
                    "Status",
                    "Comment"]

        keys=[  "name",
                "spacing",
                "manufacturer",
                "pssSensor",
                "pspSensor",
                "roh",
                "poh",
                "rightFeh",
                "leftFeh",
                "cicVersion",
                "lpgbtVersion",
                "lpgbtBandwidth",
                "moduleStatus",
                "description"]

        table = [[groupedData[name].get(key,"") for key in keys] for name in sorted(list(groupedData.keys())) ]# if name[15] =="1" ]
        print("\n")
        print ( tabulate ( table, headers= header, maxcolwidths=[None,None,None,None,None,None,None,None,None,None,29]))
        
        self.printToCsv("PS_Modules.csv", header, table)
        print ("\nOverall number of PS Modules in the Database: " + str (len(groupedData.keys()) ) + "\n")

        goodcic1 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC1" and groupedData[name].get("moduleStatus","") == "Good") ]
        badcic1 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC1" and groupedData[name].get("moduleStatus","")  in ["Bad", "Damaged"]) ]
        unknowncic1 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC1" and groupedData[name].get("moduleStatus","") == "") ]
        goodcic2 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2" and groupedData[name].get("moduleStatus","") == "Good") ]
        badcic2 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2" and groupedData[name].get("moduleStatus","")  in ["Bad", "Damaged"]) ]
        unknowncic2 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2" and groupedData[name].get("moduleStatus","") == "") ]
        goodcic21 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2.1" and groupedData[name].get("moduleStatus","") == "Good") ]
        badcic21 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2.1" and groupedData[name].get("moduleStatus","")  in ["Bad", "Damaged"]) ]
        unknowncic21 = [groupedData[name].get("name","") for name in sorted(list(groupedData.keys())) if (groupedData[name].get("cicVersion","") == "CIC2.1" and groupedData[name].get("moduleStatus","") == "") ]


        sg1 = ""
        sb1 = ""
        su1 = ""
        sg2 = ""
        sb2 = ""
        su2 = ""
        sg21 = ""
        sb21 = ""
        su21 = ""

        for module in goodcic1:
            sg1 += "\n" + module
        for module in badcic1:
            sb1 += "\n" + module
        for module in unknowncic1:
            su1 += "\n" + module
        for module in goodcic2:
            sg2 += "\n" + module
        for module in badcic2:
            sb2 += "\n" + module
        for module in unknowncic2:
            su2 += "\n" + module
        for module in goodcic21:
            sg21 += "\n" + module
        for module in badcic21:
            sb21 += "\n" + module
        for module in unknowncic21:
            su21 += "\n" + module


        moduleHeader = ["Good CIC1 Modules","Bad CIC1 Modules", "Unknown CIC1 Modules","Good CIC2 Modules","Bad CIC2 Modules", "Unknown CIC2 Modules","Good CIC2.1 Modules","Bad CIC2.1 Modules", "Unknown CIC2.1 Modules"]

        table = []
        table.append([len(goodcic1), len(badcic1), len(unknowncic1),len(goodcic2), len(badcic2), len(unknowncic2),len(goodcic21), len(badcic21), len(unknowncic21)])
        table.append([sg1,sb1,su1,sg2,sb2,su2,sg21,sb21,su21])


        moduleTable = tabulate ( table, headers= moduleHeader) 
        print( moduleTable )

    def groupPSModuleInformation( self, pData):
        moduleList = {}

        for moduleName in list( set ([ entry.get("moduleName","") for entry in  pData])):
            moduleList[moduleName] = {} 
        
        for entry in pData:
            moduleList[entry["moduleName"]]["name"]      = entry.get("moduleName","")
            moduleList[entry["moduleName"]]["spacing"]      = entry.get("spacing","")
            moduleList[entry["moduleName"]]["manufacturer"] = entry.get("manufacturer","")
            moduleList[entry["moduleName"]]["moduleStatus"] = entry.get("moduleStatus","")
            moduleList[entry["moduleName"]]["description"]  = entry.get("description","")
            if entry.get("pssSensorKindOfPart","") == "PS-s Sensor":
                moduleList[entry["moduleName"]]["pssSensor"]  = entry.get("partNameLabel","")
            if entry.get("fehKindOfPart","") == "PS Front-end Hybrid":
                if entry.get("fehHybridSide","") == "Right":
                    moduleList[entry["moduleName"]]["rightFeh"]  = entry.get("partSerialNumber","")
                elif entry.get("fehHybridSide","") == "Left":
                    moduleList[entry["moduleName"]]["leftFeh"]  = entry.get("partSerialNumber","")
                if entry.get("cicVersion","") != "":
                    moduleList[entry["moduleName"]]["cicVersion"]  = entry.get("cicVersion","")
            if entry.get("rohKindOfPart","") == "PS Read-out Hybrid":
                moduleList[entry["moduleName"]]["roh"]  = entry.get("partSerialNumber","")
                moduleList[entry["moduleName"]]["lpgbtVersion"]  = entry.get("lpgbtVersion","")
                moduleList[entry["moduleName"]]["lpgbtBandwidth"]  = entry.get("lpgbtBandwidth","")
            if entry.get("pohKindOfPart","") == "PS Power Hybrid":
                moduleList[entry["moduleName"]]["poh"]  = entry.get("partSerialNumber","")

        return moduleList

    def group2SModuleInformation( self, pData):
        moduleList = {}

        for moduleName in list( set ([ entry.get("moduleName","") for entry in  pData])):
            moduleList[moduleName] = {} 
        
        for entry in pData:
            moduleList[entry["moduleName"]]["name"]      = entry.get("moduleName","")
            moduleList[entry["moduleName"]]["spacing"]      = entry.get("spacing","")
            moduleList[entry["moduleName"]]["manufacturer"] = entry.get("manufacturer","")
            moduleList[entry["moduleName"]]["moduleStatus"] = entry.get("moduleStatus","")
            moduleList[entry["moduleName"]]["description"]  = entry.get("description","")
            if entry.get("sensorKindOfPart","") == "2S Sensor":
                if entry.get("sensorPosition","") == "TOP":
                    moduleList[entry["moduleName"]]["topSensor"]  = entry.get("partNameLabel","")
                elif entry.get("sensorPosition","") == "BOTTOM":
                    moduleList[entry["moduleName"]]["bottomSensor"]  = entry.get("partNameLabel","")
            if entry.get("fehKindOfPart","") == "2S Front-end Hybrid":
                if entry.get("fehHybridSide","") == "Right":
                    moduleList[entry["moduleName"]]["rightFeh"]  = entry.get("partSerialNumber","")
                elif entry.get("fehHybridSide","") == "Left":
                    moduleList[entry["moduleName"]]["leftFeh"]  = entry.get("partSerialNumber","")
                moduleList[entry["moduleName"]]["cicVersion"]  = entry.get("cicVersion","")
            if entry.get("sehKindOfPart","") == "2S Service Hybrid":
                moduleList[entry["moduleName"]]["seh"]  = entry.get("partSerialNumber","")
                moduleList[entry["moduleName"]]["lpgbtVersion"]  = entry.get("lpgbtVersion","")

        return moduleList

    def printToCsv(self, pFile, pHeader, pTable):
        psCsv = ""
        for entry in pHeader:
            psCsv += entry + ","
        psCsv = psCsv[:-1] + "\n"
        for line in pTable:
            for entry in line:
                psCsv += entry + ","
            psCsv = psCsv[:-1] + "\n"
        
        with open(pFile,"w") as psFile:
            psFile.write(psCsv)
            psFile.close()

    def askToStoreDBCredentials( self ):
        cli = CLIClient()
        #write system arguments expected by run()
        sys.argv = [ "python3",
                    "-n",
                    "--url=https://cmsdca.cern.ch/trk_rhapi",
                    "--login"]
        cli.run()
        return os.path.exists('.session.cache') # If a file was created login was successful (nasty workorung but does the job)

    def getMetrolgyResults( self ):
        summary = {}
        request = "select d.KIND_OF_CONDITION, d.INSERTION_USER, d.INSERTION_TIME, \
                    r.BEGIN_DATE,r.RUN_TYPE, r.DESCRIPTION, r.LOCATION, r.RUN_NUMBER, \
                    entry.PART_NAME_LABEL, entry.KIND_OF_METROLOGY, entry.ROTATION_URAD, entry.X_SHIFT_UM, entry.Y_SHIFT_UM from trker_cmsr.c9460 entry\
                    left join trker_cmsr.datasets d on d.ID = entry.CONDITION_DATA_SET_ID\
                    left join trker_cmsr.runs r on r.ID = d.RUN_ID\
                    WHERE entry.PART_NAME_LABEL LIKE '%-001%'\
                    order by entry.PART_NAME_LABEL"        
        data = self.getData(request, False)
        #print(data)
        #[print(entry) for entry in data]
        #for dataPoint in data:
        #    print (dataPoint["partNameLabel"])
        return data
    
    def printMetrologySummary( self, pData ):

        groups = []

        x = []
        y_r = []
        y_x = []
        y_y = []

        cmap=mpl.colormaps["Set1"]

        for data in pData:
            groups.append(data["partNameLabel"][8:11])

            x.append(data["partNameLabel"])
            y_r.append(data["rotationUrad"])
            y_x.append(data["xShiftUm"])
            y_y.append(data["yShiftUm"])
        
        x,y_r,y_x,y_y = zip(*sorted(zip(x,y_r,y_x,y_y), key = lambda name : (name[0][8:11] ))) #Sort by assembly site

        groups = sorted(list(set(groups)))

        y = [y_r,y_x,y_y]
        y_labels = ["Rotation (µrad)", "X Shift (µm)", "y Shift (µm)"]
        fig, axs = plt.subplots(2,2, figsize=(16, 9))
        for i, ax in enumerate(axs.flat[:-1]):
            legend_elements = []

            for j,entry in enumerate(x):
                ax.scatter([entry],[y[i][j]],  color = cmap( groups.index(entry[8:11]) /len(groups) ) )

                if j==0:
                    legend_elements.append( Line2D ( [0], [0], linewidth=0, marker='o', color=cmap( groups.index ( entry[8:11] ) / len(groups) ) ) )
                if j > 0:
                    if entry[8:11] != x[j-1][8:11]:
                        legend_elements.append( Line2D ( [0], [0], linewidth=0, marker='o', color=cmap( groups.index ( entry[8:11] ) / len(groups) ) ) )

            ax.legend(legend_elements, [group for group in groups])
            ax.grid()
            ax.set_ylabel(y_labels[i])
            for label in ax.get_xticklabels():
                label.set_rotation(45)
                label.set_ha('right')

        plt.tight_layout()
        plt.show()


    def getSensorDicingResults( self ):
        summary = {}
        request = "select d.KIND_OF_CONDITION, d.INSERTION_USER, d.INSERTION_TIME, \
                    r.BEGIN_DATE,r.RUN_TYPE, r.DESCRIPTION, r.LOCATION, r.RUN_NUMBER, \
                    entry.PART_NAME_LABEL, entry.CARDINAL_DIRECTION, entry.DISTANCE_LEFT_UM, entry.DISTANCE_RIGHT_UM from trker_cmsr.c9420 entry\
                    left join trker_cmsr.datasets d on d.ID = entry.CONDITION_DATA_SET_ID\
                    left join trker_cmsr.runs r on r.ID = d.RUN_ID\
                    WHERE (r.RUN_NUMBER>=120722 OR r.RUN_NUMBER=109438) AND r.RUN_TYPE='mod_bareSensor' \
                    order by entry.PART_NAME_LABEL"        
        data = self.getData(request, True)
        #print(data)
        [print(entry) for entry in data]
        for point in data:
            if point["partNameLabel"] not in summary:
                summary[point["partNameLabel"]] = {"x":[], "y":[]}
            summary[point["partNameLabel"]]["x"].append(point["cardinalDirection"] + " LEFT")
            summary[point["partNameLabel"]]["x"].append(point["cardinalDirection"] + " RIGHT")
            summary[point["partNameLabel"]]["y"].append(point["distanceLeftUm"])
            summary[point["partNameLabel"]]["y"].append(point["distanceRightUm"])
        [print(entry) for entry in summary]
        return summary

    def generateSensorDicingPlot( self ):
        summary = self.getSensorDicingResults()
        print(summary)
        histo = []
        means = {}
        for cd in summary[list(summary.keys())[0]]["x"]:
            means[cd]=[]
        for sensor in summary:
            for i, cd in enumerate(summary[sensor]["x"]):
                means[cd].append(summary[sensor]["y"][i])
                histo.append(summary[sensor]["y"][i])
            plt.plot(summary[sensor]["x"], summary[sensor]["y"], marker = "o", label = sensor)
        plotmeans = [np.mean(means[cd]) for cd in means]
        plt.plot(list(means.keys()) , plotmeans, color="red", linewidth="5", label = "MEAN" )
        plt.legend(ncol=3)
        plt.xlabel("Cardinal direction and corner",fontsize=18)
        plt.ylabel("Distance between round marker and sensor edge (µm)",fontsize=18)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)
        plt.ylim(230,250)
        plt.grid()
        plt.show()

        plt.hist(histo, bins = 20)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)
        plt.xlabel("Distance between round marker and sensor edge (µm)",fontsize=18)
        plt.ylabel("#",fontsize=18)

        plt.show()

    def getLastRunTypeOfModule( self ):
        pass

    def getIvCurvesOfModule( self, pNameLabel):
        query = "select pp.NAME_LABEL as MODULE_NAME, pp.A2S_SENSOR_SPACING as SPACING, part.MANUFACTURER, s.A2S_SENSOR_POSN as SENSOR_POSITION, \
                pp.ASTATUS as MODULE_STATUS, pp.DESCRIPTION, r.CHILD_NAME_LABEL AS PART_NAME_LABEL, \
                r.CHILD_SERIAL_NUMBER as PART_SERIAL_NUMBER, feh.AFE_HYBRID_SIDE as FEH_HYBRID_SIDE, feh.ACIC_VERSION as CIC_VERSION, \
                feh.KIND_OF_PART as FEH_KIND_OF_PART, seh.KIND_OF_PART as SEH_KIND_OF_PART, s.KIND_OF_PART as SENSOR_KIND_OF_PART\
                from trker_cmsr.parts part\
                inner join trker_cmsr.p9000 pp on pp.ID = part.ID \
                inner join trker_cmsr.trkr_relationships_v r on r.PARENT_NAME_LABEL=pp.NAME_LABEL\
                left join trker_cmsr.p6640 feh on r.CHILD_SERIAL_NUMBER=feh.SERIAL_NUMBER\
                left join trker_cmsr.p6660 seh on r.CHILD_SERIAL_NUMBER=seh.SERIAL_NUMBER\
                left join trker_cmsr.p1120 s on r.CHILD_NAME_LABEL=s.NAME_LABEL\
                where part.NAME_LABEL='"+pNameLabel+"'"



        query = "select * from trker_cmsr.c9500 p where p.PART_NAME_LABEL='"+pNameLabel+"'"
        data = self.getData(query)
        for entry in data:
            print(entry)
            print("\n")
        """
        IVs = {}

        for entry in data:
            if entry["conditionDataSetId"] not in [iv["conditionDataSetId"] for iv in IVs]:
                newIV[entry["conditionDataSetId"] = 
                IV.append(newIV)
            else:
        """

        #data must be grouped by the conditiondatasetid





    #r.BEGIN_DATE, r.RUN_TYPE, r.DESCRIPTION, r.LOCATION, , entry.CONDITION_DATA_SET_ID
    def test( self):
        """
        request = "select d.KIND_OF_CONDITION, d.INSERTION_USER, d.INSERTION_TIME, \
                    r.BEGIN_DATE,r.RUN_TYPE, r.DESCRIPTION, r.LOCATION,\
                    entry.PART_NAME_LABEL, entry.CARDINAL_DIRECTION, entry.DISTANCE_LEFT_UM, entry.DISTANCE_RIGHT_UM from trker_cmsr.c9420 entry\
                    left join trker_cmsr.datasets d on d.ID = entry.CONDITION_DATA_SET_ID\
                    left join trker_cmsr.runs r on r.ID = d.RUN_ID\
                    where entry.PART_NAME_LABEL = '34354_032_2-S_MAIN0'"
        """
        #request = "select * from trker_cmsr.conditions c where c.DATABASE_TABLE = 'TEST_MODULE_IV'"
        #request =  "select  from trker_int2r.p10620"


        "select pp.NAME_LABEL as MODULE_NAME, pp.APS_SENSOR_SPACING as SPACING, part.MANUFACTURER, pp.ASTATUS as MODULE_STATUS, pp.DESCRIPTION, "
        #request= "trker_int2r.p10620"

        #request =  "select module.name_label from trker_int2r.p10620 module"
        #request = "trker_int2r.kinds_of_part"
        request = "select k.* from trker_int2r.kinds_of_part k where k.NAME='PS Module' "
        request = "select * from trker_int2r.parts p where p.kind_of_part = '2S Module'"
        request = "select pp.NAME_LABEL as MODULE_NAME from trker_int2r.parts part\
                inner join trker_int2r.p10620 pp on pp.ID = part.ID"
        request = "select module.name_label, module.amodule_integration_status, module.astatus, module.acontact_points, module.a2s_sensor_spacing from trker_int2r.p10620 module" 
        #request = "select * from trker_int2r.parts part where part.kind_of_part = '2S Module'" 

        #request = "trker_cmsr.runs"# p where p.condition_Data_Set_Id='1092341'"

        request = "trker_cmsr.kinds_of_part"
        #request = "trker_int2r.trkr_attributes_v"

        data = self.getData(request, True)
        [print(d) for d in data]
    
        #print(data)  

    """
    #query = "select * from trker_cmsr.p1120 p where p.name_label='35958_004_2-S_MAIN0'"# , feh.AFE_HYBRID_SIDE                     inner join trker_cmsr.p6640 feh on r.CHILD_SERIAL_NUMBER=feh.SERIAL_NUMBER\
    #query = "select hybrid.* from trker_cmsr.p6660 hybrid"
    #query = "select * from trker_cmsr.parts p where p.ID=167160"
    #query = "trker_cmsr.test_summary_v"#.trkr_attributes_v"#.trker_sensors_v"
    #query = "select * from trker_cmsr.trkr_relationships_v p where p.PARENT_NAME_LABEL='2S_18_5_KIT-00001'"
    #query = "select p.* from trker_cmsr.parts p where p.KIND_OF_PART_ID=6640"
    #query = "select * from trker_cmsr.conditions c where c.DATABASE_TABLE = 'TEST_MODULE_IV'"
    #query = "select entry.PART_NAME_LABEL, entry.CARDINAL_DIRECTION, entry.DISTANCE_LEFT_UM, entry.DISTANCE_RIGHT_UM from trker_cmsr.c9420 entry"
    #query = "select * from trker_cmsr.c9420 entry order by entry.PART_NAME_LABEL"
    #query = "select entry.PART_NAME_LABEL, entry.CARDINAL_DIRECTION, entry.DISTANCE_LEFT_UM, entry.DISTANCE_RIGHT_UM from trker_cmsr.c9420 entry order by entry.PART_NAME_LABEL"
    #query = "select pp.NAME_LABEL, part.MANUFACTURER, pp.ASTATUS from trker_cmsr.parts part inner join trker_cmsr.p9000 pp on pp.ID = part.ID order by part.MANUFACTURER, pp.NAME_LABEL"
    #query = "select hybrid.SERIAL_NUMBER from trker_cmsr.p6640 hybrid"
    #query = "select hybrid.SERIAL_NUMBER, hybrid.ACIC_VERSION from trker_cmsr.p6640 hybrid"

    #query = "select pp.* from trker_cmsr.parts part inner join trker_cmsr.p9000 pp on pp.ID = part.ID"# order by part.MANUFACTURER, pp.NAME_LABEL"
    #query = "select p.* from trker_cmsr.parts p where p.NAME_LABEL='2S_18_6_FNL-00004' "
    #{'partTable': 'p6640', 'name': '2S Front-end Hybrid', 'description': '2S Front-end Hybrid', 'id': 6640, 'detector': 'TRKER'}
    #query = "select k.* from trker_cmsr.kinds_of_part k where k.NAME='2S Module' "
    #query = "select r.CHILD_SERIAL_NUMBER, r.PARENT_NAME_LABEL from trker_cmsr.parts part inner join trker_cmsr.trkr_relationships_v r on part.NAME_LABEL=r.PARENT_NAME_LABEL"
    """